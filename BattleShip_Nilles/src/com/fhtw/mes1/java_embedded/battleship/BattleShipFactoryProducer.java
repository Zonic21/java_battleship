package com.fhtw.mes1.java_embedded.battleship;

import com.fhtw.mes1.java_embedded.battleship.ships.BattleShipInstantiationFailed;
import com.fhtw.mes1.java_embedded.battleship.ships.IBattleShipFactory;

public class BattleShipFactoryProducer{
	public static IBattleShipFactory createBattleShipFactory(String[] initStringElems) throws ClassNotFoundException, InstantiationException, IllegalAccessException, BattleShipInstantiationFailed
	{		
		Class c = Class.forName("com.fhtw.mes1.java_embedded.battleship.ships." + initStringElems[1] + "Factory");
		try {
			IBattleShipFactory factory = (IBattleShipFactory)c.newInstance();
			return factory;
		} catch (InstantiationException e) {
			throw new BattleShipInstantiationFailed("es konte keine instanz erstellt werden", e);
		} catch (IllegalAccessException e) {
			throw new BattleShipInstantiationFailed("unbefugter zugriff", e);
		}
	}
}
