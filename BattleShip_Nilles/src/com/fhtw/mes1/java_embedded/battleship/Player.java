package com.fhtw.mes1.java_embedded.battleship;

import com.fhtw.mes1.java_embedded.battleship.ships.ICoordinate;




public class Player {
	
	Integer keynumber = 1;
	private String name;
	private BattleField battlefield = new BattleField();
 
    public void setName(String name)
    {
    	this.name = name;
    }
    

	public void createField(String name)
	{
		battlefield.readshipfile(name);
	}
	
	public boolean hitfunction(ICoordinate guess)
	{
		return battlefield.checkifhit(guess);
	}
	
	public boolean gameoverfunction()
	{
		return battlefield.checkifgameover();
	}
	

}
