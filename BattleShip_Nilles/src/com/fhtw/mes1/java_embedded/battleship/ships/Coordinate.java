package com.fhtw.mes1.java_embedded.battleship.ships;

public class Coordinate implements ICoordinate {

	public String cord = "00";
	
	public Coordinate(String position)
	{
		this.cord = position;
	}
	
	@Override
	public int getXNr() {
		char first = this.cord.charAt( 0 );				
        int x = (int) first;
        x = x - 64;
		return x;
	}

	@Override
	public int getYNr() {
		char second = this.cord.charAt( 1 );				
        int y = (int) second;
        y = y - 48;
		return y;
	}
	
	 @Override
	 public boolean equals(Object obj){
	  // Null checks not included.
	  Coordinate coordinate = (Coordinate)obj;   
	  if(this.cord.compareTo(coordinate.cord)==0)
	    return true;
	  else
	    return false;
	 }
	 
	 @Override
	 public int hashCode(){
	   return this.cord.hashCode();
	 }

}
