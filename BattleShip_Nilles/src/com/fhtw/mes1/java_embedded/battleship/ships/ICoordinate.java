package com.fhtw.mes1.java_embedded.battleship.ships;


/**
 * Interface which defines a ship coordinate on a battlefield.
 * @author mad
 *
 */
public interface ICoordinate {
	/**
	 * Returns a numeric representation of the x coordinate.
	 * @return The numeric x coordinate.
	 */
	int getXNr();
	/**
	 * Returns a numeric representation of the y coordinate.
	 * @return The numeric y coordinate.
	 */
	int getYNr();
}