package com.fhtw.mes1.java_embedded.battleship.ships;

public class HorizontalBattleShip implements IBattleShip{
	
	private String name;
	private Integer lenght;
	private Integer hitcount; 
	
	public HorizontalBattleShip(String[] initStringElems)
	{
		this.name = initStringElems[2];
		this.lenght = Integer.parseInt(initStringElems[3]);
		this.hitcount = this.lenght;
	}

	@Override
	public String getName() {
		 return this.name;
	}

	@Override
	public boolean isHit(ICoordinate leftUpperCorner, ICoordinate guess) {
		int xg = guess.getXNr();
		int yg = guess.getYNr();
		int xl = leftUpperCorner.getXNr();
		int yl = leftUpperCorner.getYNr();

		if((yg == yl) && (xg >= xl) && (xg < xl + this.lenght))
		{
			if (this.hitcount == 0)
			{
				hitcount = 1;
			}
			this.hitcount--;
			return true;
		} else {
			return false;		
		}

	}

	@Override
	public boolean isShipDestroyed() {
			if(this.hitcount == 0)
			{
				return true;
			}
			return false;
	}
}
