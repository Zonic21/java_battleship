package com.fhtw.mes1.java_embedded.battleship.ships;

public class HorizontalBattleShipFactory implements IBattleShipFactory{

	public IBattleShip create(String[] initStringElems) throws BattleShipInstantiationFailed 
	{	
		return new  HorizontalBattleShip(initStringElems);
	}
	
}
