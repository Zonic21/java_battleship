package com.fhtw.mes1.java_embedded.battleship.ships;

/**
 * This interfaces defines a method to instantiate a certain battleship implementation.
 * @author mad
 *
 */
public interface IBattleShipFactory {
	/**
	 * Creates a battleship instance. Must be implemented by a battleship provider.
	 * @param initStringElems An array of string parameters used to initialize the battleship.
	 * @return Instance of the battleship.
	 * @throws BattleShipInstantiationFailed The exception in case of an instantiation error.
	 */
	public IBattleShip create(String[] initStringElems) throws BattleShipInstantiationFailed;
}
