package com.fhtw.mes1.java_embedded.battleship.ships;

/**
 * This interface defines obligatory methods of a battleship. It must be implemented by a BattleShip class.
 * @author mad
 *
 */
public interface IBattleShip {
	/**
	 * The name of the ship like 'Bounty' :-)
	 * @return The name as String.
	 */
	String getName();
	/**
	 * Checks if the given ICoordinate guess hits the battleship.
	 * @param leftUpperCorner The absolute coordinate of the left upper corner of the battleship
	 * @param guess The absolute coordinate of the user's guess
	 * @return 	true if the ship was hit.
	 * 			false if the ship was not hit.
	 */
	boolean isHit(ICoordinate leftUpperCorner, ICoordinate guess);
	/**
	 * Checks if the ship was destroyed.
	 * @return 	true if the ship was destroyed.
	 * 			false if the ship was not destroyed.
	 */
	boolean isShipDestroyed();

}