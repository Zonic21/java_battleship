package com.fhtw.mes1.java_embedded.battleship.ships;

/**
 * This class is used to indicate an exception during battelship instantiation.
 * @author mad
 *
 */
public class BattleShipInstantiationFailed extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6202397099653992613L;

	/**
	 * Creates an instance of the exception with the given parameters.
	 * @param message The descriptive error message.
	 * @param cause The cause of the error as {@link Throwable}.
	 */
	public BattleShipInstantiationFailed(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Creates an instance of the exeption.
	 * @param message The descriptive error message.
	 */
	public BattleShipInstantiationFailed(String message) {
		super(message);
	}
}
