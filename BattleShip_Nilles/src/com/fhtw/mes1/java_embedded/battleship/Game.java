package com.fhtw.mes1.java_embedded.battleship;

import com.fhtw.mes1.java_embedded.battleship.ships.ICoordinate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.String;
import com.fhtw.mes1.java_embedded.battleship.ships.Coordinate;

public class Game {
	//Game game = new Game();

	static Player player1 = new Player();
	static Player player2 = new Player();	
	
	public static void main(String[] args){
		// TODO Auto-generated method stub

		setPlayer1(player1);
		setPlayer2(player2);
// C:\\Users\\Zonic\\workspace\\BattleShip_Nilles\\kevin.txt		
		start();

	}
	
	public static void setPlayer1(Player player) {
		BufferedReader inReader = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("dein name player1?");	
		try {
			String input = inReader.readLine();
			player.setName(input);
			player1.createField("player1");
		} catch (IOException e) {
			// TODO Auto-generated catch block
		}	
	}
	
	public static void setPlayer2(Player player) {
		BufferedReader inReader = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("dein name player2?");
		try {
			String input = inReader.readLine();
			player.setName(input);
			player2.createField("player2");
		} catch (IOException e) {
			// TODO Auto-generated catch block
		}	
	}
	
	
	public static void start() {
	
		Boolean player1GameOver = false;
		Boolean player2GameOver = false;		
		BufferedReader inReader = new BufferedReader(new InputStreamReader(System.in));
		ICoordinate guess;
		while((false == player1GameOver)&&(false == player2GameOver))
		{
			if(false == player1GameOver)
			{
				guess = checkguessisacceptable("Player1");
				boolean hit = player1.hitfunction(guess);
				if(hit == true)
				{
					System.out.println("Voll getroffen");
				}
				player1GameOver = player1.gameoverfunction();
			}
			if(false == player2GameOver)
			{
				guess = checkguessisacceptable("Player2");
				boolean hit = player2.hitfunction(guess);
				if(hit == true)
				{
					System.out.println("Voll getroffen");
				}
				player2GameOver = player2.gameoverfunction();
			}			
		}
		if (player1GameOver ==  true)
		{
			System.out.println("Player 2 hat gewonnen");	
		}else{
			System.out.println("Player 1 hat gewonnen");	
		}
			
		
	}
	
	public static ICoordinate checkguessisacceptable(String player)
	{
		BufferedReader inReader = new BufferedReader(new InputStreamReader(System.in));
		Boolean accept = false;
		ICoordinate guess = null;
		String coordinate = "00";
		while(accept == false)
		{
			System.out.println("Wohin willst du schiessen "+ player + "?");
			try {
				coordinate = inReader.readLine();
				char second = coordinate.charAt( 1 );				
		        int y = (int) second;
		        y = y - 48;
				char first = coordinate.charAt( 0 );				
		        int x = (int) first;
		        x = x - 64;
				if ((coordinate.length() !=2)||(x < 1)||(y > 7)||(x < 1)||(x > 7))
				{
					System.out.println("Bitte richtige Coordinate zwischen A1 und G7 eingeben");
				} else {
					accept = true;
				}

			} catch (IOException e) {
				e.printStackTrace();
			}	

		}
		guess = new Coordinate(coordinate);
		return guess;
		
	}
	

	  


}
