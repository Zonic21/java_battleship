package com.fhtw.mes1.java_embedded.battleship;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.fhtw.mes1.java_embedded.battleship.ships.Coordinate;
import com.fhtw.mes1.java_embedded.battleship.ships.IBattleShip;
import com.fhtw.mes1.java_embedded.battleship.ships.IBattleShipFactory;
import com.fhtw.mes1.java_embedded.battleship.ships.ICoordinate;

public class BattleField{

	    Map<ICoordinate, IBattleShip> hashMapship = new HashMap<ICoordinate, IBattleShip>();

	
		public void readshipfile(String name)
		{

				try{
	    	    	  // open input stream test.txt for reading purpose.
					BufferedReader inReader = new BufferedReader(new InputStreamReader(System.in));
					System.out.println("Wo liegt die File des " + name);
				    File datei = new File(inReader.readLine());
					String   thisLine = null;
					String[] initStringElements = new String[4];
	    	    	BufferedReader br = new BufferedReader(new FileReader(datei));
	    	    	while ((thisLine = br.readLine()) != null) 
	    	    	{	    	         	
	    	    		  initStringElements[0] = substringBefore( thisLine, ";" ); 		//coordinate
	    	    		  String thisLinetmp = substringAfter( thisLine, ";" );
	    	    		  initStringElements[1] = substringBefore( thisLinetmp, ";" ); 	//type
	    	    		  thisLinetmp = substringAfter( thisLinetmp, ";" );
	    	    		  initStringElements[2] = substringBefore( thisLinetmp, ";" ); 	//name
	    	    		  initStringElements[3] = substringAfter(thisLinetmp, ";"); 		//lenght
	    	    		  // Werte werden richtig in das String array gespeichert
	    	    		  ICoordinate leftUppercorner = new Coordinate(initStringElements[0]);
	    	    		  IBattleShipFactory factory = BattleShipFactoryProducer.createBattleShipFactory(initStringElements);
	    	    		  IBattleShip battleship = factory.create(initStringElements);
	    	    		  factory = null; // l�schen des objectes
	    	    		  hashMapship.put(leftUppercorner, battleship);	

	    	    	 } 
	    	    	// speichert 3 werte in die hashmap ab
	    	    	 br.close();
	    	      }catch(Exception e){
	    	         e.printStackTrace();
	    	      }
		}

	  public String substringBefore( String string, String delimiter )
	  {
	    int pos = string.indexOf( delimiter );

	    return pos >= 0 ? string.substring( 0, pos ) : string;
	  }

	  /**
	   * Returns the substring after the first occurrence of a delimiter. The
	   * delimiter is not part of the result.
	   * @param string    String to get a substring from.
	   * @param delimiter String to search for.
	   * @return          Substring after the last occurrence of the delimiter.
	   */
	  public String substringAfter( String string, String delimiter )
	  {
	    int pos = string.indexOf( delimiter );

	    return pos >= 0 ? string.substring( pos + delimiter.length() ) : "";
	  }
	  
	  public boolean checkifhit(ICoordinate guess)
	  {
		  
	        Iterator<ICoordinate> coordinate=hashMapship.keySet().iterator();  
	        while(coordinate.hasNext())  
	        {  
	            ICoordinate battleshipObj=coordinate.next();  
	            IBattleShip battleship=hashMapship.get(battleshipObj);
	            if (battleship.isHit(battleshipObj, guess) == true)
	            {
	            	return true;
	            }
	        }
	        return false;		  
	  }
	  public boolean checkifgameover()
	  {
	        Iterator<ICoordinate> coordinate=hashMapship.keySet().iterator(); 
			int destroycnt = hashMapship.size();
	        while(coordinate.hasNext())  
	        {  
	            ICoordinate battleshipObj=coordinate.next();  
	            IBattleShip battleship=hashMapship.get(battleshipObj);
	            if (battleship.isShipDestroyed() == true)
	            {
					System.out.println("Schiff zerst�rt");
					destroycnt--;
	            }
	        }
	        if(destroycnt == 0)
	        {
	        	return true;
	        }
	        
	        return false;	  
	  }
	

}
